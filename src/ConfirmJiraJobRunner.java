/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian;

import com.geolang.ascema.agent.jobrunner.BaseConfirmJobRunner;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Issue;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.JiraAttachment;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.JiraComment;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.AtlassianRestFactory;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.jira.JiraRest;
import com.geolang.ascema.agent.serverconnection.IEndpointClientFac;
import com.geolang.ascema.domainmodelpublic.IPatternMatchResult;
import com.geolang.ascema.endpointcommon.domain.Job;
import com.geolang.ascema.endpointcommon.messages.servernotification.ServerUIDetails;
import com.geolang.ascema.endpointcommon.parser.ParserFactory;
import com.geolang.ascema.endpointcommon.parser.ParserRecursive;
import com.geolang.ascema.endpointcommon.parser.PasswordProtectedException;
import com.geolang.ascema.endpointcommon.parser.UnprocessableFileException;
import com.geolang.ascema.endpointcommon.parser.UnprocessableReason;
import com.geolang.ascema.endpointcommon.parser.ZipUnsupportedException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.tika.metadata.Metadata;

/**
 *
 * @author Paul Woods
 */
public class ConfirmJiraJobRunner extends BaseConfirmJobRunner implements Runnable {

    private static final Logger LOG = Logger.getLogger(ConfirmJiraJobRunner.class.getName());

    public ConfirmJiraJobRunner(Job job, ServerUIDetails uidetails, IEndpointClientFac con) {
        super(job, uidetails, con);
    }

    @Override
    public void run() {
        notifyStart();
        IPatternMatchResult[] results = command.getResults();
        try ( JiraRest rest = AtlassianRestFactory.getJira()) {
            String self = command.getConfirmPath();
            if (self.contains("comment")) {
                Optional<JiraComment> comment = rest.getCommentFromUrl(self);
                if (comment.isPresent()) {
                    List<String> strings = comment.get().getAllStrings();
                    String all = strings.stream().collect(Collectors.joining(" "));
                    AtomicBoolean ok = new AtomicBoolean(true);
                    boolean OK = checkParsed(command.getFilePath(), all, results, ok.get());
                    if (!OK) {
                        ok.set(false);
                    }
                    if (ok.get()) {
                        returnOK(results);
                    } else {
                        notOK(results);
                    }
                } else {
                    //comment gone?
                    returnAllRedactedOK(results);
                }
            } else if (self.contains("attachment")) {
                ParserRecursive parser = ParserFactory.buildRecurisveParser();
                Optional<JiraAttachment> attOpt = rest.getAttachmentFromUrl(self);
                if (attOpt.isPresent()) {
                    byte[] content = rest.downloadFileContent(attOpt.get().getContentUrl());
                    AtomicBoolean ok = new AtomicBoolean(true);
                    try {
                        parser.parse(command.getFilePath(), content, (Metadata md, String parsed, String topLevel) -> {
                            boolean OK = checkParsed(command.getFilePath(), parsed, results, ok.get());
                            if (!OK) {
                                ok.set(false);
                            }
                        });
                        if (ok.get()) {
                            returnOK(results);
                        } else {
                            notOK(results);
                        }
                    } catch (UnprocessableFileException e) {
                        if (e.getReasonType().equals(UnprocessableReason.ZERO_BYTE)) {
                            returnAllRedactedOK(results);
                        } else {
                            notOK(results);
                        }
                        return;
                    } catch (PasswordProtectedException | ZipUnsupportedException ex) {
                        Logger.getLogger(ConfirmJiraJobRunner.class.getName()).log(Level.SEVERE, null, ex);
                        notOK(results);
                    }
                } else {
                    //attachment gone
                    returnAllRedactedOK(results);
                }

            } else {
                Optional<Issue> issue = rest.downloadIssueFromUrl(self);
                if (issue.isPresent()) {
                    Map<String, List<String>> strings = issue.get().getAllStrings();
                    if (strings.containsKey(command.getSubPath())) {
                        List<String> strs = strings.get(command.getSubPath());
                        String all = strs.stream().collect(Collectors.joining(" "));
                        AtomicBoolean ok = new AtomicBoolean(true);
                        boolean OK = checkParsed(command.getFilePath(), all, results, ok.get());
                        if (!OK) {
                            ok.set(false);
                        }
                        if (ok.get()) {
                            returnOK(results);
                        } else {
                            notOK(results);
                        }
                        return;
                    }

                }
                returnAllRedactedOK(results); //issue gone?
            }
        } catch (Exception ex) {
            LOG.info(ex.getMessage());
            notOK(results);
        }
    }
}
