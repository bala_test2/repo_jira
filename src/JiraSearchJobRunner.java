/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geolang.ascema.agent.jobrunner.atlassian;

import com.geolang.ascema.agent.cache.CacheManager;
import com.geolang.ascema.agent.cache.FileInfoSubPart;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.IJiraIterationCallback;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.JiraActionator;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.WorkflowHelper;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.cache.JiraCache;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.cache.JiraFileInfo;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Issue;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Project;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.ProjectCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.AtlassianRestFactory;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.NoAtlassianConnectionException;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.jira.JiraRest;
import com.geolang.ascema.agent.jobrunner.pauseresume.TopLevel;
import com.geolang.ascema.agent.serverconnection.IEndpointClient;
import com.geolang.ascema.agent.serverconnection.IEndpointClientFac;
import com.geolang.ascema.agent.settings.SettingsFile;
import com.geolang.ascema.endpointcommon.domain.BaseCommand;
import com.geolang.ascema.endpointcommon.domain.Job;
import com.geolang.ascema.endpointcommon.domain.NamedArea;
import com.geolang.ascema.endpointcommon.domain.PatternConfig;
import com.geolang.ascema.endpointcommon.domain.RetrievalResult;
import com.geolang.ascema.endpointcommon.domain.SearchResult;
import com.geolang.ascema.endpointcommon.domain.UnifiedSearchCommand;
import com.geolang.ascema.endpointcommon.domain.unifiedconfig.AtlassianSearchConfiguration;
import com.geolang.ascema.endpointcommon.domain.unifiedconfig.UnifiedSearchConfiguration;
import com.geolang.ascema.endpointcommon.messages.servernotification.ServerUIDetails;
import com.geolang.ascema.indexingcommon.endpoint.atlassian.AtlassianActionConfiguration;
import com.geolang.ascema.indexingcommon.endpoint.atlassian.JiraProject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author paulwoods
 */
public class JiraSearchJobRunner extends BaseAtlassianSearchJobRunner implements IJiraIterationCallback {

    private static final Logger LOG = Logger.getLogger(JiraSearchJobRunner.class.getName());

    private final AtlassianSearchConfiguration config;

    private final Collection<PatternConfig> patternConfigs;
    private final JiraCache cache;
    private String reportedDeviceName = "";
    private final TopLevel topLevel;

    public JiraSearchJobRunner(Job job, ServerUIDetails uidetails, IEndpointClientFac con, TopLevel topLevel) {
        super(job, uidetails, con);

        reportedDeviceName = SettingsFile.getInstance().getAtlassianHost();
        this.deviceDescription = reportedDeviceName;
        this.cache = CacheManager.getInstance().getJiraCache();
        this.topLevel = topLevel;
        UnifiedSearchCommand command = (UnifiedSearchCommand) job.command();
        UnifiedSearchConfiguration uConf = command.getConfig();
        this.config = uConf.getAtlassian();
        this.patternConfigs = config.getPatterns();
    }

    @Override
    public void callback(Issue issue, JiraFileInfo fi, String email, NamedArea area, boolean onlySearchFileNamePatterns, JiraRest rest) {

        List<SearchResult> toSend = getMatches((FileInfoSubPart subPart, String partName) -> {
            byte[] content = "".getBytes();
            if (fi.isIsAttachment()) {

                Optional<String> opt = fi.getDownloadLink();
                if (opt.isPresent()) {
                    content = rest.downloadFileContent(opt.get());
                }
            } else {
                content = subPart.toString().getBytes();
            }

            RetrievalResult rr = new RetrievalResult(BaseCommand.CommandType.ATLASSIAN, managerUUID, job, reportedDeviceName, fi.getPath(), fi.getFileDates(), partName, subPart.getInfo(), area, fi.getPermissions());
            rr.setContent(content);
            rr.setStoredFilename(generateStoredFilename(partName, fi));
            rr.setIsEmail(false);
            return Optional.of(rr);
        }, fi, patternConfigs, area, email, isRunningRetrievalJob, BaseCommand.CommandType.ATLASSIAN, onlySearchFileNamePatterns);

        if (!toSend.isEmpty()) {
            if (!isRunningRetrievalJob) {
                doAction(issue, rest);
            }
            Optional<IEndpointClient> client = con.getClientOrWait();
            if (client.isPresent()) {
                for (SearchResult res : toSend) {
                    boolean ok = client.get().sendResult(res);
                    if (!ok) {
                        LOG.log(Level.WARNING, "add result failed");
                        //TODO store these? or can messaging service do that
                    }
                }
            }

        }

    }

    @Override
    public void run() {
        Thread.currentThread().setName("Jira Search Runner " + job.getTaskInstanceId());
        LOG.info("Starting jira search");
        try ( JiraRest rest = AtlassianRestFactory.getJira()) {

            WorkflowHelper wf = new WorkflowHelper(rest, config.isSearchJiraWorkflows(), this);

            if (config.isSearchJira()) {
                addJiraJobs(rest, wf);
            }

            executor.shutdown();
            try {
                executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
                LOG.log(Level.INFO, "Finished Jira Search");

            } catch (InterruptedException e) {
                LOG.severe(e.getMessage());
            }

        } catch (NoAtlassianConnectionException ex) {
            Logger.getLogger(JiraSearchJobRunner.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOG.info("Finished Jira");
    }

    private void addJiraJobs(JiraRest rest, WorkflowHelper helper) {

        LOG.log(Level.INFO, "Start Jira Search");
        List<Project> rootProjects = new ArrayList<>();

        if (this.config.isSearchAllProjects()) {
            Optional<ProjectCollection> projects = Optional.empty();
            projects = rest.getAllProjects();

            if (!projects.isPresent()) {
                Logger.getLogger(JiraSearchJobRunner.class.getName()).log(Level.WARNING, "Failed to get projects");
                return;
            }
            while (projects.isPresent()) {
                projects.get().getProjects().forEach(p -> {
                    rootProjects.add(p);
                });
                if (!projects.get().isIsLast()) {
                    projects = rest.getMore(projects.get());
                } else {
                    projects = Optional.empty();
                }
            }

        } else {
            String deviceId = SettingsFile.getInstance().getUUID();
            for (JiraProject jp : config.getSpecificProjects()) {
                if (jp.getDeviceId().equals(deviceId)) {
                    Optional<Project> proj = rest.getProjectFromId(jp.getId());
                    if (proj.isPresent()) {
                        rootProjects.add(proj.get());
                    }
                }
            }
        }
        LOG.log(Level.INFO, "Found {0} projects", rootProjects.size());
        for (Project p : rootProjects) {

            JiraRootFolderIterator iter = new JiraRootFolderIterator(rest, cache, p, sleepBetweenFilesMs, this, config, topLevel.addOrGetRootFolder(p.getKey()), helper);
            allJobs.add(Pair.of(executor.submit(iter), iter));
        }

    }

    @Override
    public void onPasswordProtected(String path) {
        onPasswordProtected(BaseCommand.CommandType.ATLASSIAN, path, managerUUID, reportedDeviceName);
    }

    @Override
    public void onUnprocessableFile(String filepath, String reason) {
        onUnprocessable(BaseCommand.CommandType.ATLASSIAN, filepath, managerUUID, reportedDeviceName, reason);
    }

    private String generateStoredFilename(String partName, JiraFileInfo info) {
        if (info.isIsAttachment()) {
            return partName;
        }
        return partName + ".txt";

    }

    private void doAction(Issue issue, JiraRest rest) {
        if (!this.job.isPerformActionAutomatically()) {
            return;
        }
        if (issue == null) {
            return;
        }
        AtlassianActionConfiguration actConfig = config.getActionConfiguration();
        if (!actConfig.getJiraActions().isEmpty()) {
            JiraActionator actionator = new JiraActionator();
            actionator.doActionFromIssue(issue, actConfig.getJiraActions(), rest);
        }
    }
}
