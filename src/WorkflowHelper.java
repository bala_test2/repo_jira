/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira;

import com.geolang.ascema.agent.cache.FileInfoSubPart;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.cache.JiraFileInfo;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Project;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Transition;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Workflow;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.WorkflowCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.WorkflowScheme;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.WorkflowSchemeCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.jira.JiraRest;
import com.geolang.ascema.domainmodelpublic.pattern.MimeType;
import com.geolang.ascema.endpointcommon.domain.CloudPlatformAreaType;
import com.geolang.ascema.endpointcommon.domain.FileDates;
import com.geolang.ascema.endpointcommon.domain.NamedArea;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

/**
 *
 * @author Paul Woods
 */
public class WorkflowHelper {

    private static final Logger LOG = Logger.getLogger(WorkflowHelper.class.getName());

    // workflow rules are all over the place
    //the list of workflow schemes is on the project
    // transitions are on the issues
    //workflows are contained in schemes, but are fetched separately
    //to get the properties of a workflow you need the transition id?
    //https://docs.atlassian.com/software/jira/docs/api/REST/8.19.0/#workflow-getProperties
    private Set<String> checkedTransitions = new HashSet<>();
    private Set<Integer> checkedSchemes = new HashSet<>();
    private final List<Workflow> allWorkflows;
    private final boolean enabled;
    private final IJiraIterationCallback callback;

    public WorkflowHelper(JiraRest rest, boolean enabled, IJiraIterationCallback callback) {
        this.enabled = enabled;
        this.callback = callback;
        if (enabled) {
            Optional<WorkflowCollection> colOpt = rest.getWorkflows();
            if (colOpt.isPresent()) {
                allWorkflows = colOpt.get().getWorkflows();
            } else {
                LOG.warning("failed to fetch workflows");
                allWorkflows = new ArrayList<>();
            }
            for (Workflow w : allWorkflows) {
                Map<String, String> allStr = w.getAllStrings();
                callbackWithStrings("Workflow", allStr, w.getName(), w.getName(), w.getLastModifiedUser(), "", rest);
            }
            if (!rest.isOnPrem()) {
                Optional<WorkflowSchemeCollection> schemeOpt = rest.getAllWorkflowSchemes();
                if (schemeOpt.isPresent()) {
                    for (WorkflowScheme ws : schemeOpt.get().getSchemes()) {
                        Map<String, String> allStr = ws.getAllStrings();
                        callbackWithStrings("Workflow Scheme", allStr, ws.getName(), Integer.toString(ws.getId()), "unknown", "", rest);
                    }
                }
            }
        } else {
            allWorkflows = new ArrayList<>();
        }
    }

    //TODO call this for each issue and get the transition properties?
    public synchronized void checkTransition(List<Transition> transitions, JiraRest rest) {
        if (!enabled) {
            return;
        }
        for (Transition t : transitions) {
            if (!checkedTransitions.contains(t.getId())) {
          //      rest.getTransitionProperties(t);
                Map<String, String> allStr = t.getAllStrings();
                callbackWithStrings("Workflow transition", allStr, t.getName(), t.getId(), "unknown", "", rest);
                checkedTransitions.add(t.getId());
            }
        }
    }

    public synchronized void processScheme(Project project, String projectAdminEmail, WorkflowScheme scheme, JiraRest rest) {
        if (!enabled || !rest.isOnPrem()) {
            return;
        }
        if (!checkedSchemes.contains(scheme.getId())) {
            Map<String, String> allStr = scheme.getAllStrings();
            callbackWithStrings("Workflow Scheme", allStr, scheme.getName(), Integer.toString(scheme.getId()), "unknown", projectAdminEmail, rest);
            checkedSchemes.add(scheme.getId());
        }

    }

    private void callbackWithStrings(String type, Map<String, String> allStr, String name, String id, String creator, String adminEmail, JiraRest rest) {
        if (callback != null) {
            JiraFileInfo info = new JiraFileInfo(id, creator,
                    creator, id, type + " " + name, new FileDates(),
                    0, id, MimeType.TEXT, new HashSet<>(), adminEmail, false);

            Set<FileInfoSubPart> subParts = new HashSet<>();
            for (Map.Entry<String, String> entry : allStr.entrySet()) {
                subParts.add(new FileInfoSubPart(info, entry.getKey(), entry.getValue()));
            }
            info.setSubParts(subParts);
            callback.callback(null,info, adminEmail, NamedArea.fromCloud(CloudPlatformAreaType.ATLASSIAN_JIRA), false, rest);
        }
    }

}
