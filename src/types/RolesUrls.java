/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class RolesUrls {

    private String adminUrl;
    private Map<String, String> otherUrls = new HashMap<>();

    public RolesUrls(JSONObject obj) {
        for (String s : obj.keySet()) {
            if (s.equals("Administrators")) {
                adminUrl = obj.optString(s);
            } else {
                otherUrls.put(s, obj.getString(s));
            }
        }

    }

    public Optional<String> getAdminUrl() {
        if (adminUrl == null || adminUrl.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(adminUrl);
    }

    public Map<String, String> getOtherUrls() {
        return otherUrls;
    }

}
