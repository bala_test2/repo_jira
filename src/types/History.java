/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class History {

    private final String id;
    private JiraUser author;
    private String created;
    private List<HistoryItem> items = new ArrayList<>();

    public History(JSONObject obj) {
        id = obj.optString("id");
        JSONObject auth = obj.optJSONObject("author");
        if (auth != null) {
            author = new JiraUser(auth);
        }
        created = obj.optString("created");
        JSONArray arr = obj.optJSONArray("items");
        if (arr != null) {
            for (int i = 0; i < arr.length(); i++) {
                items.add(new HistoryItem(arr.getJSONObject(i)));
            }
        }
    }

    public List<String> getAllStrings() {
        List<String> ret = new ArrayList<>();
        for (HistoryItem item : items) {
            if (item.getFromString() != null && !item.getFromString().isEmpty()) {
                ret.add(item.getFromString());
            }
        }
        return ret;
    }

    public String getId() {
        return id;
    }

    public JiraUser getAuthor() {
        return author;
    }

    public String getCreated() {
        return created;
    }

    public List<HistoryItem> getItems() {
        return items;
    }

}
