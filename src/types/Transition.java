/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class Transition {

    private String self;
    private String description;
    private String name;
    private String id;

    public Transition(JSONObject obj) {
        self = obj.optString("self");
        description = obj.optString("description");
        name = obj.optString("name");
        id = obj.optString("id");
    }

    public String getSelf() {
        return self;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public Map<String, String> getAllStrings() {
        Map<String, String> ret = new HashMap<>();
        if (name != null && !name.isEmpty()) {
            ret.put("name", name);
        }
        if (description != null && !description.isEmpty()) {
            ret.put("description", description);
        }
        return ret;
    }

}
