/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class ProjectCollection extends Pagination {

    private final List<Project> projects = new ArrayList<>();

    //constructor for on-prem
    public ProjectCollection(JSONArray arr) {
        super(true);
        for (int i = 0; i < arr.length(); i++) {
            JSONObject val = arr.getJSONObject(i);
            projects.add(new Project(val));
        }

    }

    //cloud
    public ProjectCollection(JSONObject obj) {
        super(obj);
        JSONArray arr = obj.optJSONArray("values");
        if (arr != null) {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject val = arr.getJSONObject(i);
                projects.add(new Project(val));
            }
        }
    }

    @Override
    public int getSize() {
        return projects.size();
    }

    @Override
    public ProjectCollection create(JSONObject obj) {
        return new ProjectCollection(obj);
    }

    public List<Project> getProjects() {
        return projects;
    }

}
