/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class JiraAttachmentCollection extends Pagination {

    private final List<JiraAttachment> attachments = new ArrayList<>();

    public JiraAttachmentCollection(JSONObject obj) {
        super(obj);
        JSONArray arr = obj.optJSONArray("values");
        if (arr != null) {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject c = arr.getJSONObject(i);
                attachments.add(new JiraAttachment(c));
            }
        }
    }

    @Override
    public int getSize() {
        return attachments.size();
    }

    @Override
    public JiraAttachmentCollection create(JSONObject obj) {
        return new JiraAttachmentCollection(obj);
    }

    public List<JiraAttachment> getAttachments() {
        return attachments;
    }

}
