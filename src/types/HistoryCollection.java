/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class HistoryCollection extends Pagination {

    private final List<History> histories = new ArrayList<>();

    public HistoryCollection(JSONObject obj) {
        super(obj);
        JSONArray historiesObj = obj.optJSONArray("histories");
        if (historiesObj != null) {
            for (int i = 0; i < historiesObj.length(); i++) {
                histories.add(new History(historiesObj.getJSONObject(i)));
            }
        }
    }

    public Map<String, List<String>> getAllStrings() {
        Map<String, List<String>> ret = new HashMap<>();
        for (History h : histories) {
            ret.put("HistoryLog " + h.getCreated(), h.getAllStrings());
        }
        return ret;
    }

    @Override
    public int getSize() {
        return histories.size();
    }

    @Override
    public HistoryCollection create(JSONObject obj) {
        return new HistoryCollection(obj);
    }

}
