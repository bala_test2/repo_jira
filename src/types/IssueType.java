/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class IssueType {

    String id;
    String name;
    boolean subTask;

    public IssueType(JSONObject obj) {
        id = obj.optString("id");
        name = obj.optString("name");
        subTask = obj.optBoolean("subtask");
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isSubTask() {
        return subTask;
    }

    @Override
    public String toString() {
        return "IssueType{" + "id=" + id + ", name=" + name + ", subTask=" + subTask + '}';
    }

}
