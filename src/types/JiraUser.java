/*
 *  Copyright (C)  Geolang Ltd 2018
 * This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.io.Serializable;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class JiraUser implements Serializable, Comparable<JiraUser> {

    private String accountId;
    private final String accountType;
    private String email;
    private final String displayName;
    private final String key;
    private final boolean active;

    public JiraUser(JSONObject obj) {
        accountId = obj.optString("accountId");
        if (accountId == null || accountId.isEmpty()) {
            accountId = obj.optString("name");
        }
        accountType = obj.optString("accountType");
        email = obj.optString("email");
        if (email == null || email.isEmpty()) {
            email = obj.optString("emailAddress");
        }
        displayName = obj.getString("displayName");
        active = obj.optBoolean("active");
        key = obj.optString("key");
    }

    public String getAccountId() {
        return accountId;
    }

    public String getAccountType() {
        return accountType;
    }

    public String getEmail() {

        return email;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean isActive() {
        return active;
    }

    public String getKey() {
        return key;
    }

    @Override
    public int compareTo(JiraUser o) {
        return getDisplayName().compareTo(o.getDisplayName());
    }

    @Override
    public String toString() {
        return "JiraUser{" + "accountId=" + accountId + ", accountType=" + accountType + ", email=" + email + ", displayName=" + displayName + ", active=" + active + '}';
    }

}
