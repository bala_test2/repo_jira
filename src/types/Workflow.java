/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class Workflow {

    private String name;
    private String description;
    private String lastModifiedUser; // display name
    private String lastModifiedDate; //"Yesterday 11:25 AM",
    private int steps;

    public Workflow(JSONObject obj) {
        name = obj.optString("name");
        description = obj.optString("description");
        lastModifiedUser = obj.optString("lastModifiedUser");
        lastModifiedDate = obj.optString("lastModifiedDate");
        steps = obj.optInt("steps");
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getLastModifiedUser() {
        return lastModifiedUser;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public int getSteps() {
        return steps;
    }

    public Map<String, String> getAllStrings() {
        Map<String, String> ret = new HashMap<>();
        if (name != null && !name.isEmpty()) {
            ret.put("name", name);
        }
        if (description != null && !description.isEmpty()) {
            ret.put("description", description);
        }
        return ret;
    }

}
