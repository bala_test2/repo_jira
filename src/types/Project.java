/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.io.Serializable;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class Project implements Serializable {

    private final String self;
    private final String id;
    private final String key;
    private final String name;
    private final String projectTypeKey;
    private final boolean isPrivate;
    private final JiraUser lead;

    public Project(JSONObject obj) {
        self = obj.optString("self");
        id = obj.optString("id");
        key = obj.optString("key");
        name = obj.optString("name");
        projectTypeKey = obj.optString("projectTypeKey");
        isPrivate = obj.optBoolean("isPrivate");
        JSONObject leadObj = obj.optJSONObject("lead");
        if (leadObj == null) {
            lead = null;
        } else {
            lead = new JiraUser(leadObj);
        }
    }

    public String getProjectTypeKey() {
        return projectTypeKey;
    }

    public boolean isIsPrivate() {
        return isPrivate;
    }

    public String getSelf() {
        return self;
    }

    public String getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public JiraUser getLead() {
        return lead;
    }

}
