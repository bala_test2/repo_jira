/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class JiraComment {

    private final String self;
    private final String id;
    private JiraUser author;
    private final String created;
    private final String updated;
    private String bodyType;
    private int bodyVersion;
    private final List<FieldContent> bodyContents = new ArrayList<>();
    private String bodyText = null;//on prem

    public JiraComment(JSONObject obj) {
        self = obj.optString("self");
        id = obj.optString("id");
        created = obj.optString("created");
        updated = obj.optString("updated");
        JSONObject body = obj.optJSONObject("body");
        if (body != null) {
            bodyType = body.optString("type");
            bodyVersion = body.optInt("version");
            JSONArray bodyC = body.optJSONArray("content");
            if (bodyC != null) {
                for (int i = 0; i < bodyC.length(); i++) {
                    bodyContents.add(new FieldContent(bodyC.getJSONObject(i)));
                }
            }
        } else {
            bodyText = obj.optString("body");
        }
        JSONObject auth = obj.optJSONObject("author");
        if (auth != null) {
            author = new JiraUser(auth);
        }
    }

    public String getSelf() {
        return self;
    }

    public String getId() {
        return id;
    }

    public JiraUser getAuthor() {
        return author;
    }

    public String getBodyType() {
        return bodyType;
    }

    public int getBodyVersion() {
        return bodyVersion;
    }

    public List<FieldContent> getBodyContents() {
        return bodyContents;
    }

    public String getCreated() {
        return created;
    }

    public String getUpdated() {
        return updated;
    }

    public List<String> getAllStrings() {
        List<String> ret = new ArrayList<>();

        for (FieldContent fc : bodyContents) {
            ret.addAll(fc.getTexts());
        }
        if (bodyText != null) {
            ret.add(bodyText);
        }
        return ret;
    }

}
