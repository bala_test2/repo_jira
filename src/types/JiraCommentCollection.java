/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class JiraCommentCollection extends Pagination {

    private List<JiraComment> comments = new ArrayList<>();

    public JiraCommentCollection(JSONObject obj) {
        super(obj);
        JSONArray arr = obj.optJSONArray("comments");
        if (arr != null) {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject c = arr.getJSONObject(i);
                comments.add(new JiraComment(c));
            }
        }
    }

    @Override
    public int getSize() {
        return comments.size();
    }

    @Override
    public JiraCommentCollection create(JSONObject obj) {
        return new JiraCommentCollection(obj);
    }

    public List<JiraComment> getComments() {
        return comments;
    }

}
