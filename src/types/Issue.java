/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import com.geolang.ascema.agent.jobrunner.atlassian.rest.jira.JiraRest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class Issue {

    private final String self;
    private final String id;
    private final String key;
    private String statuscategorychangedate;
    private IssueType issuetype;
    private Description description = null;
    private String created;
    private String updated;
    private List<String> labels = new ArrayList<>();
    private JiraUser creator;
    private JiraUser reporter;
    private Map<String, String> otherFields = new HashMap<>();
    private List<Transition> transitions = new ArrayList<>();
    private List<JiraAttachment> attachments = new ArrayList<>();
    private List<JiraComment> comments = new ArrayList<>();
    private HistoryCollection history;
    private Map<String, String> names = new HashMap<>();

    public Issue(JSONObject obj, JiraRest rest) {
        self = obj.optString("self");
        id = obj.optString("id");
        key = obj.optString("key");

        JSONArray arr = obj.optJSONArray("transitions");
        if (arr != null) {
            for (int i = 0; i < arr.length(); i++) {
                transitions.add(new Transition(arr.getJSONObject(i)));
            }
        }
        JSONObject changelog = obj.optJSONObject("changelog");
        if (changelog != null) {
            history = new HistoryCollection(changelog);
        }

        JSONObject namesObj = obj.optJSONObject("names");
        if (namesObj != null) {
            for (String key : namesObj.keySet()) {
                String val = namesObj.getString(key);
                names.put(key, val);
            }
        }
        JSONObject fields = obj.optJSONObject("fields");

        fields.keys()
                .forEachRemaining(key -> {
                    switch (key) {
                        case "attachment" -> {
                            JSONArray optObj = fields.optJSONArray("attachment");
                            if (optObj != null && !optObj.isEmpty()) {
                                for (int i = 0; i < optObj.length(); i++) {
                                    attachments.add(new JiraAttachment(optObj.getJSONObject(i)));
                                }
                            }
                        }
                        case "comment" -> {
                            JSONObject optObj = fields.optJSONObject("comment");
                            if (optObj != null) {
                                JiraCommentCollection coll = new JiraCommentCollection(optObj);
                                while (coll != null) {
                                    comments.addAll(coll.getComments());
                                    if (coll.isIsLast()) {
                                        coll = null;
                                    } else {
                                        Optional<JiraCommentCollection> opt = rest.getMore(coll);
                                        if (opt.isPresent()) {
                                            coll = opt.get();
                                        } else {
                                            coll = null;
                                        }
                                    }
                                }
                            }
                        }
                        case "statuscategorychangedate" ->
                            statuscategorychangedate = fields.getString(key);
                        case "issuetype" ->
                            issuetype = new IssueType(fields.getJSONObject(key));
                        case "created" ->
                            created = fields.optString(key);
                        case "updated" ->
                            updated = fields.optString(key);
                        case "labels" -> {
                            JSONArray labelsArr = fields.getJSONArray(key);
                            for (int j = 0; j < labelsArr.length(); j++) {
                                labels.add(labelsArr.getString(j));
                            }
                        }
                        case "description" -> {
                            JSONObject desc = fields.optJSONObject(key);
                            if (desc != null) {
                                description = new Description(desc);
                            } else {
                                String descOnPrem = fields.optString(key);
                                if (descOnPrem != null) {
                                    description = new Description(descOnPrem);
                                }
                            }
                        }
                        case "creator" -> {
                            JSONObject c = fields.optJSONObject(key);
                            if (c != null) {
                                creator = new JiraUser(c);
                            }
                        }
                        case "reporter" -> {
                            JSONObject c = fields.optJSONObject(key);
                            if (c != null) {
                                reporter = new JiraUser(c);
                            }
                        }

                        case "status", "priority", "watches", "project" -> {
                        }
                        default -> {
                            String f = fields.optString(key);
                            if (f != null && !f.isEmpty() && !f.equals("[]")) {
                                otherFields.put(key, f);
                            }
                        }
                    }
                    //probably don't need to parse these objects
                }
                );

    }

    public String getSelf() {
        return self;
    }

    public String getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public List<Transition> getTransitions() {
        return transitions;
    }

    public String getStatuscategorychangedate() {
        return statuscategorychangedate;
    }

    public IssueType getIssuetype() {
        return issuetype;
    }

    public Description getDescription() {
        return description;
    }

    public String getCreated() {
        return created;
    }

    public String getUpdated() {
        return updated;
    }

    public List<String> getLabels() {
        return labels;
    }

    public JiraUser getCreator() {
        return creator;
    }

    public JiraUser getReporter() {
        return reporter;
    }

    public Map<String, String> getOtherFields() {
        return otherFields;
    }

    public Map<String, List<String>> getAllStrings() {
        Map<String, List<String>> ret = new HashMap<>();
        if (description != null) {
            ret.put("description", description.getAllDescriptionStrings());
        }
        for (String key : otherFields.keySet()) {
            ret.put(translateName(key), Collections.singletonList(otherFields.get(key)));
        }
        if (history != null) {
            ret.putAll(history.getAllStrings());
        }
        return ret;
    }

    public List<JiraAttachment> getAttachments() {
        return attachments;
    }

    public List<JiraComment> getComments() {
        return comments;
    }

    public HistoryCollection getHistory() {
        return history;
    }

    public Map<String, String> getNames() {
        return names;
    }

    public String translateName(String in) {
        if (names.containsKey(in)) {
            return names.get(in);
        }
        return in;
    }

    @Override
    public String toString() {
        return "Issue{" + "self=" + self + ", id=" + id + ", key=" + key + ", statuscategorychangedate=" + statuscategorychangedate + ", issuetype=" + issuetype + ", description=" + description + ", created=" + created + ", updated=" + updated + ", labels=" + labels + ", creator=" + creator + ", otherFields=" + otherFields + '}';
    }

}
