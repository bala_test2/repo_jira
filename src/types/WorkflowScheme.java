/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class WorkflowScheme {

    private int id;
    private String name;
    private String description;
    private String defaultWorkflow;
    private boolean draft;
    private String self;

    public WorkflowScheme(JSONObject obj) {
        id = obj.optInt("id");
        name = obj.optString("name");
        description = obj.optString("description");
        defaultWorkflow = obj.optString("defaultWorkflow");
        draft = obj.optBoolean("draft");
        self = obj.optString("self");
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDefaultWorkflow() {
        return defaultWorkflow;
    }

    public boolean isDraft() {
        return draft;
    }

    public String getSelf() {
        return self;
    }

    public Map<String, String> getAllStrings() {
        Map<String, String> ret = new HashMap<>();
        if (name != null && !name.isEmpty()) {
            ret.put("name", name);
        }
        if (description != null && !description.isEmpty()) {
            ret.put("description", description);
        }
        return ret;
    }
}
