/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.io.Serializable;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public abstract class Pagination implements Serializable {

    private String self;
    private final int startAt;
    private final int maxResults;
    private final int total;
    private final boolean isLast;

    public Pagination(boolean isLast){
        this.isLast = isLast;
        startAt =0;
        maxResults=0;
        total=0;
        self ="";
    }
    public Pagination(JSONObject obj) {
        self = obj.optString("self");
        startAt = obj.optInt("startAt");
        maxResults = obj.optInt("maxResults");
        total = obj.optInt("total");
        isLast = obj.optBoolean("isLast");

    }
    public abstract int getSize();
    public abstract <T extends Pagination> T create(JSONObject obj);
    public int getStartAt() {
        return startAt;
    }

    public int getMaxResults() {
        return maxResults;
    }

    public int getTotal() {
        return total;
    }

    public boolean isIsLast() {
        return isLast;
    }

    public String getSelf() {
        return self;
    }

}
