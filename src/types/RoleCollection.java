/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class RoleCollection {

    private final List<String> userIds = new ArrayList<>();
    private final List<String> groupNames = new ArrayList<>();

    public RoleCollection(JSONObject obj) {
        JSONArray arr = obj.optJSONArray("actors");
        if (arr != null) {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject actor = arr.getJSONObject(i);
                if (actor.has("actorGroup")) {
                    JSONObject userObj = actor.getJSONObject("actorGroup");
                    String name = userObj.optString("name");
                    if (name != null) {
                        groupNames.add(name);
                    }
                } else if (actor.has("actorUser")) {
                    JSONObject userObj = actor.getJSONObject("actorUser");
                    String accountId = userObj.optString("accountId");
                    if (accountId != null) {
                        userIds.add(accountId);
                    }
                } else if (actor.has("type")) {//on-prem
                    String type = actor.optString("type");
                    if (type != null) {
                        if (type.equals("atlassian-group-role-actor")) {
                            String name = actor.optString("name");
                            if (name != null && !name.isEmpty()) {
                                groupNames.add(name);
                            }
                        } else if (type.equals("atlassian-user-role-actor")) {
                            String name = actor.optString("name");
                            if (name != null && !name.isEmpty()) {
                                userIds.add(name);
                            }
                        }

                    }
                }
            }

        }
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public List<String> getGroupNames() {
        return groupNames;
    }

}
