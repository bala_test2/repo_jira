/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class Description {

    private int version;
    private String type;
    private List<FieldContent> contents = new ArrayList<>();
    private String onPremDescription = null;

    public Description(String desc) {
        onPremDescription = desc;
    }

    public Description(JSONObject obj) {
        version = obj.optInt("version");
        type = obj.optString("type");
        JSONArray content = obj.optJSONArray("content");
        if (content != null) {
            for (int i = 0; i < content.length(); i++) {
                contents.add(new FieldContent(content.getJSONObject(i)));
            }
        }
    }

    public int getVersion() {
        return version;
    }

    public String getType() {
        return type;
    }

    public List<FieldContent> getContents() {
        return contents;
    }

    List<String> getAllDescriptionStrings() {
        List<String> ret = new ArrayList<>();
        contents.forEach(dc -> {
            ret.addAll(dc.getTexts());
        });
        if (onPremDescription != null) {
            ret.add(onPremDescription);
        }
        return ret;
    }

    @Override
    public String toString() {
        return "Description{" + "version=" + version + ", type=" + type + ", contents=" + contents + '}';
    }

}
