/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class FieldContent {

    private String type;
    private List<String> texts = new ArrayList<>();

    public FieldContent(JSONObject obj) {
        type = obj.optString("type");
        JSONArray arr = obj.optJSONArray("content");
        if (arr != null) {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject c = arr.getJSONObject(i);
                if (c.has("text")) {
                    texts.add(c.getString("text"));
                }
            }
        }
    }

    public String getType() {
        return type;
    }

    public List<String> getTexts() {
        return texts;
    }
    
    
    /*
  	"description": {
			"version": 1,
			"type": "doc",
			"content": [{
				"type": "paragraph",
				"content": [{
					"type": "text",
					"text": "blah description  Credit Card MasterCard 5123724390621007 "
				}]
			}, {
				"type": "mediaGroup",
				"content": [{
					"type": "media",
					"attrs": {
						"id": "071c3360-21dc-4f62-a1fa-9156d34ca636",
						"type": "file",
						"collection": ""
					}
				}]
			}, {
				"type": "paragraph",
				"content": [{
					"type": "text",
					"text": " "
				}]
			}]
		},
     */

    @Override
    public String toString() {
        return "DescriptionContent{" + "type=" + type + ", texts=" + texts + '}';
    }
}
