/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class JiraUserCollection extends Pagination {

    private List<JiraUser> users = new ArrayList<>();

    public JiraUserCollection(JSONObject obj) {
        super(obj);
        JSONArray arr = obj.optJSONArray("values");
        if (arr != null) {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject val = arr.getJSONObject(i);
                users.add(new JiraUser(val));
            }
        }
    }

    @Override
    public int getSize() {
        return users.size();
    }

    @Override
    public JiraUserCollection create(JSONObject obj) {
        return new JiraUserCollection(obj);
    }

    public List<JiraUser> getUsers() {
        return users;
    }

}
