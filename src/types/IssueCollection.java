/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import com.geolang.ascema.agent.jobrunner.atlassian.rest.jira.JiraRest;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class IssueCollection {

    private final List<Issue> issues = new ArrayList<>();
    private final int startAt;
    private final int maxResults;
    private final int total;
    private final String key;

    public IssueCollection(String key, JSONObject obj, JiraRest rest) {
        this.key = key;
        startAt = obj.optInt("startAt");
        maxResults = obj.optInt("maxResults");
        total = obj.optInt("total");
        JSONArray arr = obj.optJSONArray("issues");
        if (arr != null) {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject val = arr.getJSONObject(i);
                issues.add(new Issue(val, rest));
            }
        }
    }

    public String getKey() {
        return key;
    }

    public int getSize() {
        return issues.size();
    }

    public List<Issue> getIssues() {
        return issues;
    }

    public int getStartAt() {
        return startAt;
    }

    public int getMaxResults() {
        return maxResults;
    }

    public int getTotal() {
        return total;
    }

    public boolean isDone() {
        return getSize() + getStartAt() >= getTotal();
    }

}
