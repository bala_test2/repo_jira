/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class HistoryItem {

    private String field;
    private String fieldtype;
    private String fromString;
    private String toString;

    public HistoryItem(JSONObject obj) {
        field = obj.optString("field");
        fieldtype = obj.optString("fieldtype");
        fromString = obj.optString("fromString");
        toString = obj.optString("toString");
    }

    public String getField() {
        return field;
    }

    public String getFieldtype() {
        return fieldtype;
    }

    public String getFromString() {
        return fromString;
    }

    public String getToString() {
        return toString;
    }

}
