/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class WorkflowSchemeCollection extends Pagination {

    List<WorkflowScheme> schemes = new ArrayList<>();

    public WorkflowSchemeCollection(JSONObject obj) {
        super(obj);
        JSONArray arr = obj.optJSONArray("values");
        if (arr != null) {
            for (int i = 0; i < arr.length(); i++) {
                schemes.add(new WorkflowScheme(arr.getJSONObject(i)));
            }
        }
    }

    @Override
    public int getSize() {
        return schemes.size();
    }

    @Override
    public WorkflowSchemeCollection create(JSONObject obj) {
        return new WorkflowSchemeCollection(obj);
    }

    public List<WorkflowScheme> getSchemes() {
        return schemes;
    }

}
