/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.types;

import java.util.Optional;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class JiraAttachment {

    private final String self;
    private final String id;
    private final String filename;
    private final String created;
    private JiraUser author;
    private final long size;
    private final String mimeType;
    private final String contentUrl;

    public JiraAttachment(JSONObject obj) {
        self = obj.optString("self");
        id = obj.optString("id");
        filename = obj.optString("filename");
        created = obj.optString("created");
        size = obj.optLong("size");
        mimeType = obj.optString("mimeType");
        contentUrl = obj.optString("content");
        JSONObject auth = obj.optJSONObject("author");
        if (auth != null) {
            author = new JiraUser(auth);
        }

    }

    public String getSelf() {
        return self;
    }

    public String getId() {
        return id;
    }

    public String getFilename() {
        return filename;
    }

    public String getCreated() {
        return created;
    }

    public JiraUser getAuthor() {
        return author;
    }

    public long getSize() {
        return size;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public Optional<String> getDownloadLink() {
        if (contentUrl == null) {
            return Optional.empty();
        }
        return Optional.of(contentUrl);
    }

}
