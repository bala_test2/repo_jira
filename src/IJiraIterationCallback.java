/*
 * Copyright (C)  Geolang Ltd 2017
 * This file is part of the Ascema project
 * Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira;

import com.geolang.ascema.agent.jobrunner.atlassian.jira.cache.JiraFileInfo;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Issue;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.jira.JiraRest;
import com.geolang.ascema.endpointcommon.domain.NamedArea;

/**
 *
 * @author paulwoods
 */
public interface IJiraIterationCallback {

    public void callback(Issue issue,JiraFileInfo fi, String email, NamedArea area, boolean onlySearchFileNamePatterns, JiraRest rest);

    public void onPasswordProtected(String path);

    public void onUnprocessableFile(String filepath,String reason);
}
