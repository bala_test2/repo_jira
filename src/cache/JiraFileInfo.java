/*
 * Copyright (C)  Geolang Ltd 2017
 * This file is part of the Ascema project
 * Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.cache;

import com.geolang.ascema.agent.cache.FileInfoBase;
import com.geolang.ascema.domainmodelpublic.pattern.MimeType;
import com.geolang.ascema.endpointcommon.domain.FileDates;
import com.geolang.ascema.endpointcommon.domain.FilePermission;
import java.util.Optional;
import java.util.Set;

/**
 *
 * @author paulwoods
 */
public class JiraFileInfo extends FileInfoBase {

    private static final long serialVersionUID = 1L;

    private final String id;
    private final boolean isAttachment;
    private final String topLevelEmailAddress;
    private String downloadLink = null;

    public JiraFileInfo(String id, String lastModifiedBy, String owner, String confirmPath, String path, FileDates filedates, long size, String name,
            MimeType mimeType, Set<FilePermission> permissions, String spaceAdminEmail, boolean isAttachment) {
        super(confirmPath, path, name, filedates, size, lastModifiedBy, owner, mimeType, permissions);

        this.id = id;
        this.topLevelEmailAddress = spaceAdminEmail;
        this.isAttachment = isAttachment;
    }

    public boolean isIsAttachment() {
        return isAttachment;
    }

    @Override
    public String getTopLevelEmailAddress() {
        return topLevelEmailAddress;
    }

    public Optional<String> getDownloadLink() {
        if (downloadLink == null) {
            return Optional.empty();
        }
        return Optional.of(downloadLink);
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

}
