/*
 * Copyright (C)  Geolang Ltd 2018
 * This file is part of the Ascema project
 * Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira.cache;

import com.geolang.ascema.agent.cache.CacheBase;
import com.geolang.ascema.agent.cache.CacheManager;
import com.geolang.ascema.agent.cache.FileInfoSubPart;
import com.geolang.ascema.agent.filesystem.ExtensionMatcher;
import com.geolang.ascema.agent.filesystem.cache.FileInfoCache;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.JiraAttachment;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Project;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.jira.JiraRest;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.jira.JiraUserGroupSpaceCache;
import com.geolang.ascema.agent.settings.SettingsFile;
import com.geolang.ascema.domainmodelpublic.pattern.MimeType;
import com.geolang.ascema.endpointcommon.domain.FileDates;
import com.geolang.ascema.endpointcommon.domain.FilePermission;
import com.geolang.ascema.endpointcommon.parser.PasswordProtectedException;
import com.geolang.ascema.endpointcommon.parser.UnprocessableFileException;
import com.geolang.ascema.endpointcommon.parser.ZipUnsupportedException;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.tika.exception.EncryptedDocumentException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;

/**
 *
 * @author arjun ponnusamy
 */
public class JiraCache extends CacheBase<JiraFileInfo> {

    private static final Logger LOG = Logger.getLogger(JiraCache.class.getName());

    private final boolean TRACE_LOG = SettingsFile.getInstance().getLogAllFiles();
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss.SSS");

    public JiraCache() {
        super(CacheManager.JIRA_NAME, false);
    }

    @Override
    public String getName() {
        return CacheManager.JIRA_NAME;
    }

    public Optional<JiraFileInfo> getFileInfo(JiraAttachment attach, String issuePath, ExtensionMatcher extensionMatcher, JiraRest rest, Project project, String spaceAdminEmail) throws UnprocessableFileException, PasswordProtectedException {

        //long filesize = content.getFilesize();
        String key = attach.getId();
        String path = issuePath + "/" + attach.getFilename();
        byte[] contents = null;

        Optional<JiraFileInfo> value = get(key);
        JiraFileInfo ret;
        AtomicBoolean justGetContent = new AtomicBoolean(false);

        if (value.isPresent()) {
            //seen in cache
            ret = value.get();
            if (!ret.supportedMimeType(extensionMatcher)) {
                return Optional.of(ret); //nothing further -we have seen this and it isn't anything we can deal with
            }
            Instant cachedDate = ret.getFileDates().getModified();
            Instant currentDate;
            try {
                currentDate = dateFormat.parse(attach.getCreated()).toInstant();
            } catch (ParseException ex) {
                Logger.getLogger(JiraCache.class.getName()).log(Level.SEVERE, null, ex.getMessage());
                currentDate = Instant.now();
            }
            if (cachedDate.equals(currentDate)) {
                //not modified since last time we saw it
                if (ret.hasContent()) {
                    //and we already have the content
                    return Optional.of(ret);
                } else {
                    justGetContent.set(true); //in cache - not modified, we just need to populate the content again
                }
            }
        } else {
            //not in cache                

            MimeType type;
            Optional<String> opt = attach.getDownloadLink();
            if (opt.isPresent()) {
                contents = rest.downloadFileContent(opt.get());

                try {
                    type = detector.getType(contents, attach.getFilename());
                } catch (FileSystemException ex) {
                    LOG.log(Level.WARNING, ex.getMessage());
                    type = MimeType.INVALID;
                } catch (IOException ex) {
                    Logger.getLogger(FileInfoCache.class.getName()).log(Level.SEVERE, null, ex);
                    LOG.log(Level.WARNING, ex.getMessage());
                    type = MimeType.INVALID;
                }
            } else {
                type = MimeType.INVALID; //no content
            }
            Instant lastModified;
            try {
                lastModified = dateFormat.parse(attach.getCreated()).toInstant();
            } catch (ParseException ex) {
                Logger.getLogger(JiraCache.class.getName()).log(Level.SEVERE, null, ex.getMessage());
                lastModified = Instant.now();
            }
            Instant created;
            try {
                created = dateFormat.parse(attach.getCreated()).toInstant();
            } catch (ParseException ex) {
                Logger.getLogger(JiraCache.class.getName()).log(Level.SEVERE, null, ex.getMessage());
                created = Instant.now();
            }
            FileDates filedates = new FileDates(created, lastModified, null);
            long length = contents == null ? 0 : contents.length;
            Set<FilePermission> permissions = JiraUserGroupSpaceCache.getPermissionsForProject(project, rest);

            ret = new JiraFileInfo(attach.getId(), attach.getAuthor().getDisplayName(),
                    attach.getAuthor().getDisplayName(),
                    attach.getSelf(), path, filedates, length,
                    attach.getFilename(), type, permissions, spaceAdminEmail, true);
        }

        //either not in the cache OR cache is out of date OR cache is OK but we don't have the content 
        if (ret.supportedMimeType(extensionMatcher)) {

            Optional<String> opt = attach.getDownloadLink();
            if (opt.isPresent()) {
                if (contents == null || contents.length == 0) {
                    contents = rest.downloadFileContent(opt.get());
                }
            }
            //get file content

            try {
                if (contents != null) {

                    AtomicInteger totalLength = new AtomicInteger(0);
                    List<FileInfoSubPart> parts = new ArrayList<>();
                    parser.parse(key, contents, (Metadata md, String parsed, String topLevel) -> {
                        String[] anyException = md.getValues(TikaCoreProperties.TIKA_META_EXCEPTION_EMBEDDED_STREAM);
                        if (anyException != null && anyException.length > 0) {
                            throw new EncryptedDocumentException(attach.getFilename());
                        }
                        totalLength.addAndGet(parsed.length());
                        Optional<FileInfoSubPart> part = ret.createSubPart(md, parsed);
                        if (part.isPresent()) {
                            part.get().setLink(attach.getContentUrl());
                            parts.add(part.get());
                            if (TRACE_LOG) {
                                LOG.log(Level.INFO, "Parsed {0}", part.get().getSubPath());
                            }
                        }
                    });
                    if (totalLength.get() < MAX_STRING_LENGTH_IN_CACHE) {
                        ret.setSubParts(parts);
                        put(key, ret);
                    } else {
                        if (!justGetContent.get()) {
                            put(key, ret);
                        }
                        ret.setSubParts(parts);
                    }
                }
            } catch (OutOfMemoryError e) {
                LOG.log(Level.SEVERE, "Out of Memory. Failed to get  info for {0} - {1}", new Object[]{attach.getFilename(), e.getLocalizedMessage()});
                throw new UnprocessableFileException(e);
            } catch (ZipUnsupportedException ex) {
                Logger.getLogger(JiraCache.class.getName()).log(Level.SEVERE, null, ex);
                throw new UnprocessableFileException(ex);
            }

        } else {
            //not a text file - just note it
            put(key, ret);
        }

        return Optional.of(ret);

    }

}
