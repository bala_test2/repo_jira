/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian;

import com.geolang.ascema.agent.cache.FileInfoSubPart;
import com.geolang.ascema.agent.filesystem.BaseIterator;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.IJiraIterationCallback;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.WorkflowHelper;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.cache.JiraCache;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.cache.JiraFileInfo;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Issue;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.IssueCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.JiraAttachment;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.JiraComment;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Project;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.WorkflowScheme;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.jira.JiraRest;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.jira.JiraUserGroupSpaceCache;
import com.geolang.ascema.agent.jobrunner.pauseresume.SearchFolderState;
import com.geolang.ascema.agent.settings.SettingsFile;
import com.geolang.ascema.domainmodelpublic.pattern.MimeType;
import com.geolang.ascema.endpointcommon.domain.CloudPlatformAreaType;
import com.geolang.ascema.endpointcommon.domain.FileDates;
import com.geolang.ascema.endpointcommon.domain.FilePermission;
import com.geolang.ascema.endpointcommon.domain.NamedArea;
import com.geolang.ascema.endpointcommon.domain.unifiedconfig.AtlassianSearchConfiguration;
import com.geolang.ascema.endpointcommon.parser.PasswordProtectedException;
import com.geolang.ascema.endpointcommon.parser.UnprocessableFileException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paul Woods
 */
public class JiraRootFolderIterator extends BaseIterator implements Runnable {

    private static final Logger LOG = Logger.getLogger(JiraRootFolderIterator.class.getName());

    private final Project project;
    private final JiraRest rest;
    private final JiraCache cache;
    private String projectAdminEmail;
    private final boolean TRACE_LOG = SettingsFile.getInstance().getLogAllFiles();
    private final IJiraIterationCallback callback;
    private final AtlassianSearchConfiguration config;
    private final WorkflowHelper workflowHelper;

    JiraRootFolderIterator(JiraRest rest, JiraCache cache, Project p, int sleepBetweenFilesMs,
            IJiraIterationCallback callback, AtlassianSearchConfiguration config, SearchFolderState progress, WorkflowHelper workflowHelper) {
        super(sleepBetweenFilesMs, config, progress);
        this.project = p;
        this.rest = rest;
        this.cache = cache;
        this.callback = callback;
        this.config = config;
        this.workflowHelper = workflowHelper;
    }

    @Override
    public String getLoggingName() {
        return project.getName();
    }

    @Override
    public void run() {
        if (searchState.isComplete()) {
            LOG.log(Level.INFO, "Already processed, skipping project {0}", this.project.getName());
            return;
        }
        Thread.currentThread().setName("Search project " + this.project.getName());
        Optional<String> emailOpt = JiraUserGroupSpaceCache.getAdminEmailForJiraProject(project, rest);
        if (emailOpt.isPresent()) {
            projectAdminEmail = emailOpt.get();
        }
        if (this.config.isSearchJiraWorkflows()) {
            Optional<WorkflowScheme> scheme = rest.getWorkflowSchemes(project);
            if (scheme.isPresent()) {
                workflowHelper.processScheme(project, projectAdminEmail, scheme.get(), rest);
            }
        }
        Optional<IssueCollection> colOpt = rest.getIssuesForProject(project.getId());
        while (colOpt.isPresent()) {
            IssueCollection collection = colOpt.get();
            for (Issue issue : collection.getIssues()) {
                processIssue(issue);
                if (stop) {
                    break;
                }
            }
            if (!collection.isDone() && !stop) {
                colOpt = rest.getMoreIssues(collection);
            } else {
                colOpt = Optional.empty();
            }
        }
        if (!stop) {
            searchState.setFolderSearchFinished(true);
        }
        Thread.currentThread().setName("Finished project " + this.project.getName());
    }

    private void processIssue(Issue issue) {

        String modificationDate = issue.getUpdated();
        String createdDate = issue.getCreated();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss.SSS");
        Instant ftModify;
        if (modificationDate != null && !modificationDate.isEmpty()) {
            try {
                ftModify = dateFormat.parse(modificationDate).toInstant();
            } catch (ParseException ex) {
                Logger.getLogger(JiraRootFolderIterator.class.getName()).log(Level.SEVERE, null, ex.getMessage());
                ftModify = Instant.now();
            }
        } else {
            ftModify = Instant.now();
        }

        Instant ftCreate;
        if (createdDate != null && !createdDate.isEmpty()) {
            try {
                ftCreate = dateFormat.parse(modificationDate).toInstant();
            } catch (ParseException ex) {
                Logger.getLogger(JiraRootFolderIterator.class.getName()).log(Level.SEVERE, null, ex.getMessage());
                ftCreate = Instant.now();
            }
        } else {
            ftCreate = Instant.now();
        }

        boolean dateOk = datesOK(ftModify, ftCreate);
        Map<String, List<String>> strings = issue.getAllStrings();

        long size = 0;
        for (String key : strings.keySet()) {
            for (String val : strings.get(key)) {
                size += val.length();
            }
        }
        if (dateOk) {
            Set<FilePermission> permissions = JiraUserGroupSpaceCache.getPermissionsForProject(project, rest);
            FileDates filedates = new FileDates(ftCreate, ftModify, null);
            try {
                if (config.isSearchJiraIssues()) {

                    JiraFileInfo info = new JiraFileInfo(issue.getSelf(), issue.getCreator().getDisplayName(),
                            issue.getCreator().getDisplayName(), issue.getSelf(), project.getName() + "/" + issue.getKey(), filedates,
                            size, issue.getKey(), MimeType.TEXT, permissions, projectAdminEmail, false);

                    Set<FileInfoSubPart> subParts = new HashSet<>();
                    for (Entry<String, List<String>> entry : issue.getAllStrings().entrySet()) {
                        for (String val : entry.getValue()) {
                            FileInfoSubPart fis = new FileInfoSubPart(info, entry.getKey(), val);
                            fis.setLink(generateLink(issue));
                            subParts.add(fis);
                        }
                    }
                    info.setSubParts(subParts);
                    if (stop) {
                        return;
                    }
                    callback.callback(issue, info, issue.getCreator().getEmail(), NamedArea.fromCloud(CloudPlatformAreaType.ATLASSIAN_JIRA), false, rest);
                    searchState.fileProcessed(size);
                }
                if (config.isSearchJiraComments()) {
                    for (JiraComment comment : issue.getComments()) {
                        JiraFileInfo info = new JiraFileInfo(comment.getSelf(), comment.getAuthor().getDisplayName(),
                                comment.getAuthor().getDisplayName(), comment.getSelf(), project.getName() + "/" + issue.getKey() + " comment " + comment.getId(), filedates,
                                size, issue.getKey(), MimeType.TEXT, permissions, projectAdminEmail, false);

                        Set<FileInfoSubPart> subParts = new HashSet<>();
                        for (String s : comment.getAllStrings()) {
                            FileInfoSubPart fis = new FileInfoSubPart(info, "comment", s);

                            fis.setLink(generateLink(issue, comment));
                            subParts.add(fis);
                        }
                        info.setSubParts(subParts);
                        if (stop) {
                            return;
                        }
                        searchState.fileProcessed(comment.getAllStrings().stream().mapToInt(String::length).sum());
                        callback.callback(issue, info, issue.getCreator().getEmail(), NamedArea.fromCloud(CloudPlatformAreaType.ATLASSIAN_JIRA), false, rest);
                    }
                }
                if (config.isSearchJiraAttachments()) {
                    for (JiraAttachment attach : issue.getAttachments()) {
                        String name = attach.getFilename();
                        searchState.fileProcessed(attach.getSize());
                        boolean onlyFileNames = false;
                        if (!extensionMatcher.matches(name)) { // file extensions
                            if (!filenamePatternMatcher.matches(name)) {
                                continue;
                            }
                            onlyFileNames = true;
                        }
                        Optional<JiraFileInfo> fi = cache.getFileInfo(attach, project.getName() + "/" + issue.getKey(), extensionMatcher, rest, project, projectAdminEmail);
                        if (!stop && fi.isPresent() && fi.get().supportedMimeType(extensionMatcher)) {
                            callback.callback(issue, fi.get(), attach.getAuthor().getEmail(), NamedArea.fromCloud(CloudPlatformAreaType.ATLASSIAN_JIRA), onlyFileNames, rest);
                        }

                    }

                }
            } catch (PasswordProtectedException ex) {
                callback.onPasswordProtected(issue.getKey());

            } catch (UnprocessableFileException ex) {
                callback.onUnprocessableFile(issue.getKey(), ex.getLocalizedMessage());

            }
        }

    }

    private String generateLink(Issue issue) {
        return SettingsFile.getInstance().getAtlassianHost() + "/browse/" + issue.getKey();
    }

    private String generateLink(Issue issue, JiraComment comment) {
        //http://10.10.2.98:8080/browse/PAUL-1?focusedCommentId=10100&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-10100
        return SettingsFile.getInstance().getAtlassianHost() + "/browse/" + issue.getKey() + "?focusedCommentId=" + comment.getId();
    }
}
