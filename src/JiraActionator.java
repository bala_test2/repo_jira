/*
 * Copyright (C)  Geolang Ltd 2018
 * This file is part of the Ascema project
 * Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.jira;

import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Issue;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.AtlassianRestFactory;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.NoAtlassianConnectionException;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.jira.JiraRest;
import com.geolang.ascema.indexingcommon.endpoint.atlassian.JiraAction;
import com.geolang.ascema.indexingcommon.endpoint.atlassian.JiraActionType;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author paulwoods
 */
public class JiraActionator {

    private static final Logger LOG = Logger.getLogger(JiraActionator.class.getName());

    public JiraActionator() {

    }

    public void doActionFromIssue(Issue issue, List<JiraAction> actions, JiraRest rest) {
        for (JiraAction act : actions) {
            if (act.getType().equals(JiraActionType.LABEL)) {
                rest.addLabel(issue, act.getValue());
            }
        }
    }

    public void doActionFromPathAndConfirmPath(List<JiraAction> actions, String path, String confirmInfo) {
        try ( JiraRest rest = AtlassianRestFactory.getJira()) {
            Optional<Issue> issueOpt = rest.downloadIssueFromUrl(confirmInfo);
            if (issueOpt.isPresent()) {
                Issue issue = issueOpt.get();
                doActionFromIssue(issue, actions, rest);
            } else {
                LOG.log(Level.WARNING, "Failed to find issue from {0}", confirmInfo);
            }
        } catch (NoAtlassianConnectionException ex) {
            Logger.getLogger(JiraActionator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
