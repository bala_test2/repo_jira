/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.rest.jira;

import com.geolang.ascema.agent.jobrunner.atlassian.confluence.NotFoundException;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.JiraUser;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.ProjectCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.BasicAuthAtlassianToken;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.confluence.ConfluenceRest;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class JiraRestOnPrem extends JiraRest {

    private static final Logger LOG = Logger.getLogger(JiraRestOnPrem.class.getName());

    public JiraRestOnPrem(String host, BasicAuthAtlassianToken token) {
        super(host, token, "");
    }

    @Override
    public boolean isOnPrem() {
        return true;
    }

    @Override
    public final boolean login() { // can we download projects
        String url = baseUrl + context + "/project";

        HttpGet get = new HttpGet(url);
        addRequestHeaders(get);

        try ( CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();  CloseableHttpResponse response = httpClient.execute(get)) {

            //apparently this can happen
            Header anyErr = response.getFirstHeader("X-Seraph-LoginReason");
            if (anyErr != null && anyErr.getValue().equals("AUTHENTICATION_DENIED")) {
                lasterror = "API asked for CAPTCHA, you need to log in via a browser";
                return false;
            }

            if (response.getStatusLine().getStatusCode() != 200) {
                if (response.getStatusLine().getStatusCode() == 404) {
                    lasterror = "Failed to login, bad url? " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase();
                } else {
                    lasterror = "Failed to login, bad username or password?" + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase();
                }
                return false;
            }

            return true;
        } catch (UnknownHostException e) {
            lasterror = "Unknow Host " + e.getMessage();
        } catch (IOException ex) {
            Logger.getLogger(ConfluenceRest.class.getName()).log(Level.SEVERE, null, ex);
            lasterror = "Unknow error " + ex.getMessage();
        }
        return false;

    }

    @Override
    public Optional<ProjectCollection> getAllProjects() {
        try {

            String url = baseUrl + context + "/project";
            String json = executeGet(url);

            if (json.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new ProjectCollection(new JSONArray(json)));
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    @Override
    public Optional<JiraUser> getUserFromAccountId(String accountId) {
        String url = baseUrl + context + "/user/?username=" + accountId;
        try {

            String json = this.executeGet(url);
            if (!json.isEmpty()) {
                JiraUser usr = new JiraUser(new JSONObject(json));
                return Optional.of(usr);
            }
        } catch (NotFoundException ex) {
            Logger.getLogger(ConfluenceRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            LOG.warning(ex.getLocalizedMessage());
        }
        return Optional.empty();
    }
    
}
