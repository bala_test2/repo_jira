/*
 *  Copyright (C)  Geolang Ltd 2018
 * This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.rest.jira;

import com.geolang.ascema.agent.jobrunner.atlassian.confluence.types.UserProfile;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.JiraUser;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.JiraUserCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Project;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.ProjectCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.RoleCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.RolesUrls;
import com.geolang.ascema.agent.settings.SettingsFile;
import com.geolang.ascema.endpointcommon.domain.FilePermission;
import com.geolang.ascema.indexingcommon.endpoint.atlassian.JiraProject;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class JiraUserGroupSpaceCache {

    private static final Logger LOG = Logger.getLogger(JiraUserGroupSpaceCache.class.getName());

    private static final Cache<String, Set<FilePermission>> projectToFilePermission;
    private static final Cache<String, ProjectRoles> projectToRoleCollection;
    private static final Cache<String, Set<JiraUser>> groupToUsersCache;
    private static final Cache<String, Optional<JiraUser>> accountIdToUserCache;
    private static final Cache<String, Set<JiraProject>> projectCache;
    private static final Cache<String, Optional<String>> projectIdToEmailCache;

    static {
        projectToFilePermission = CacheBuilder.newBuilder()
                .expireAfterWrite(1, TimeUnit.HOURS)
                .build();
        projectToRoleCollection = CacheBuilder.newBuilder()
                .expireAfterWrite(1, TimeUnit.HOURS)
                .build();

        groupToUsersCache = CacheBuilder.newBuilder()
                .expireAfterWrite(2, TimeUnit.HOURS)
                .build();
        accountIdToUserCache = CacheBuilder.newBuilder()
                .expireAfterWrite(12, TimeUnit.HOURS)
                .build();

        projectIdToEmailCache = CacheBuilder.newBuilder()
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .build();
        projectCache = CacheBuilder.newBuilder()
                .expireAfterWrite(2, TimeUnit.HOURS)
                .build();
    }

    public static void clearCaches() {
        LOG.info("Clear JiraUserGroupSpaceCache");
        groupToUsersCache.invalidateAll();
        projectToRoleCollection.invalidateAll();
        projectCache.invalidateAll();
        projectIdToEmailCache.invalidateAll();
        accountIdToUserCache.invalidateAll();
    }

    public static ProjectRoles getProjectRoles(Project proj, JiraRest rest) {
        try {
            return projectToRoleCollection.get(proj.getKey(), () -> {
                Optional<RolesUrls> urls = rest.getProjectRolesUrls(proj.getKey());
                if (urls.isEmpty()) {
                    return new ProjectRoles(proj.getKey());
                }
                Map<String, RoleCollection> ret = new HashMap<>();
                RolesUrls roleUrls = urls.get();
                Optional<String> adminUrl = roleUrls.getAdminUrl();
                if (adminUrl.isPresent()) {
                    Optional<RoleCollection> rolesColOpt = rest.getUsersWithRole(adminUrl.get());
                    if (rolesColOpt.isPresent()) {
                        ret.put(ProjectRoles.ADMIN_ROLE, rolesColOpt.get());
                    }
                }
                for (String name : roleUrls.getOtherUrls().keySet()) {
                    Optional<RoleCollection> rolesColOpt = rest.getUsersWithRole(roleUrls.getOtherUrls().get(name));
                    if (rolesColOpt.isPresent()) {
                        ret.put(name, rolesColOpt.get());
                    }
                }
                return new ProjectRoles(proj.getKey(), ret);
            });
        } catch (ExecutionException ex) {
            Logger.getLogger(JiraUserGroupSpaceCache.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOG.log(Level.WARNING, "Failed to fetch roles for {0}", proj.getKey());
        return new ProjectRoles(proj.getKey());
    }

    public static Set<JiraProject> getAllProjects(JiraRest rest) {
        SettingsFile settings = SettingsFile.getInstance();
        try {
            return projectCache.get("BOB", () -> {
                Set<JiraProject> ret = new HashSet<>();

                Optional<ProjectCollection> projOpt = rest.getAllProjects();

                while (projOpt.isPresent()) {
                    ProjectCollection projectCollection = projOpt.get();
                    for (Project p : projectCollection.getProjects()) {
                        ret.add(new JiraProject(p.getId(), p.getKey(), p.getName(), p.getProjectTypeKey(), p.isIsPrivate(),
                                settings.getAtlassianHost(), settings.getUUID()));
                    }

                    if (!projectCollection.isIsLast()) {
                        projOpt = rest.getMore(projectCollection);
                    } else {
                        projOpt = Optional.empty();
                    }
                }
                return ret;
            });
        } catch (ExecutionException ex) {
            Logger.getLogger(JiraUserGroupSpaceCache.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Collections.EMPTY_SET;
    }

    public static Optional<JiraUser> getUserFromAccountId(JiraRest rest, String accountId) {
        try {
            return accountIdToUserCache.get(accountId, () -> {
                return rest.getUserFromAccountId(accountId);
            });
        } catch (ExecutionException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    public static Set<JiraUser> getUsersInGroup(JiraRest rest, String id) {
        // rest.getUsersInGroup(g.getId());
        try {
            return groupToUsersCache.get(id, () -> {
                Set<JiraUser> users = new HashSet<>();
                Optional<JiraUserCollection> userOpt = rest.getUsersInGroup(id);
                while (userOpt.isPresent()) {
                    for (JiraUser u : userOpt.get().getUsers()) {
                        accountIdToUserCache.put(u.getAccountId(), Optional.of(u));
                    }
                    users.addAll(userOpt.get().getUsers());
                    if (!userOpt.get().isIsLast()) {
                        userOpt = rest.getMore(userOpt.get());
                    } else {
                        userOpt = Optional.empty();
                    }
                }

                return users;
            });
        } catch (ExecutionException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return Collections.EMPTY_SET;
    }

    public static void removeUser(UserProfile up) {
        //todo
    }

    public static JiraUser fromJson(JSONObject obj) {
        JiraUser u = new JiraUser(obj);
        return u;
    }

    public static Optional<String> getAdminEmailForJiraProject(Project project, JiraRest rest) {
        try {
            return projectIdToEmailCache.get(project.getId(), () -> {
                return loadAdminEmailForProject(project, rest);
            });
        } catch (ExecutionException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    public static Set<FilePermission> getPermissionsForProject(Project project, JiraRest rest) {
        try {
            return projectToFilePermission.get(project.getKey(), () -> {
                Set<FilePermission> ret = new HashSet<>();
                ProjectRoles projectRoles = getProjectRoles(project, rest);
                for (String name : projectRoles.getRoles().keySet()) {
                    RoleCollection col = projectRoles.getRoles().get(name);
                    List<String> userIds = col.getUserIds();
                    for (String user : userIds) {
                        Optional<JiraUser> ju = getUserFromAccountId(rest, user);
                        if (ju.isPresent()) {
                            JiraUser u = ju.get();
                            ret.add(FilePermission.newUser(u.getEmail(), u.getDisplayName(), u.getAccountId()));
                        }
                    }
                    for (String groupName : col.getGroupNames()) {
                        ret.add(FilePermission.newDepartment(groupName));
                    }
                }
                return ret;
            });
        } catch (ExecutionException ex) {
            Logger.getLogger(JiraUserGroupSpaceCache.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new HashSet<>();
    }

    private static Optional<String> loadAdminEmailForProject(Project project, JiraRest rest) {
        Optional<String> ret = Optional.empty();
        ProjectRoles projectRoles = getProjectRoles(project, rest);
        RoleCollection adminRoles = projectRoles.getRoles().get(ProjectRoles.ADMIN_ROLE);
        if (adminRoles == null) {
            LOG.log(Level.WARNING, "No admin roles for {0}", project.getKey());
            return ret;
        }
        List<String> userIds = adminRoles.getUserIds();
        List<String> emails = new ArrayList<>();
        for (String user : userIds) {
            Optional<JiraUser> ju = getUserFromAccountId(rest, user);
            if (ju.isPresent()) {
                emails.add(ju.get().getEmail());
            }
        }
        for (String groupName : adminRoles.getGroupNames()) {
            Set<JiraUser> usersInGroup = getUsersInGroup(rest, groupName);
            for (JiraUser ju : usersInGroup) {
                emails.add(ju.getEmail());
            }
        }
        String emailsConcat = emails.stream().collect(Collectors.joining(","));
        return Optional.of(emailsConcat);
    }
}
