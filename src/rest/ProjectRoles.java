/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.rest.jira;

import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.RoleCollection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Paul Woods
 */
public class ProjectRoles {

    private final String projectKey;
    private final Map<String, RoleCollection> roles;
    static String ADMIN_ROLE = "Administrators";

    public ProjectRoles(String projectKey) {
        this.projectKey = projectKey;
        this.roles = new HashMap<>();
    }

    public ProjectRoles(String projectKey, Map<String, RoleCollection> roles) {
        this.projectKey = projectKey;
        this.roles = roles;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public Map<String, RoleCollection> getRoles() {
        return roles;
    }

}
