/*
 *  Copyright (C)  Geolang Ltd 2021
 *  This file is part of the Ascema project
 *  Ascema can not be copied and/or distributed without the express permission of Geolang Ltd
 */
package com.geolang.ascema.agent.jobrunner.atlassian.rest.jira;

import com.geolang.ascema.agent.jobrunner.atlassian.confluence.NotFoundException;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Issue;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.IssueCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.JiraAttachment;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.JiraComment;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.JiraUser;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.JiraUserCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Pagination;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Project;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.ProjectCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.RoleCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.RolesUrls;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.Transition;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.WorkflowCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.WorkflowScheme;
import com.geolang.ascema.agent.jobrunner.atlassian.jira.types.WorkflowSchemeCollection;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.BasicAuthAtlassianToken;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.RestBase;
import com.geolang.ascema.agent.jobrunner.atlassian.rest.confluence.ConfluenceRest;
import com.google.common.net.UrlEscapers;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Paul Woods
 */
public class JiraRest extends RestBase {

    private static final Logger LOG = Logger.getLogger(JiraRest.class.getName());
    protected String lasterror = "";
    protected final String context;
    private final static String BASE_PATH = "/rest/api/latest";

    public JiraRest(String host, BasicAuthAtlassianToken token) {
        super(host, token);
        this.context = "" + BASE_PATH; //cloud context
    }

    //constructor for self hosted atlassian 
    protected JiraRest(String host, BasicAuthAtlassianToken token, String context) {
        super(host, token);
        if (!context.isEmpty()) {
            if (context.startsWith("/")) {
                this.context = context + BASE_PATH;
            } else {
                this.context = "/" + context + BASE_PATH;
            }
        } else {
            this.context = BASE_PATH;
        }

    }

    public boolean isOnPrem() {
        return false;
    }

    public boolean login() { // can we download projects
        String url = baseUrl + context + "/project/search";

        HttpGet get = new HttpGet(url);
        addRequestHeaders(get);

        try ( CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();  CloseableHttpResponse response = httpClient.execute(get)) {

            //apparently this can happen
            Header anyErr = response.getFirstHeader("X-Seraph-LoginReason");
            if (anyErr != null && anyErr.getValue().equals("AUTHENTICATION_DENIED")) {
                lasterror = "API asked for CAPTCHA, you need to log in via a browser";
                return false;
            }

            if (response.getStatusLine().getStatusCode() != 200) {
                if (response.getStatusLine().getStatusCode() == 404) {
                    lasterror = "Failed to login, bad url? " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase();
                } else {
                    lasterror = "Failed to login, bad username or password?" + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase();
                }
                return false;
            }

            return true;
        } catch (UnknownHostException e) {
            lasterror = "Unknow Host " + e.getMessage();
        } catch (IOException ex) {
            Logger.getLogger(ConfluenceRest.class.getName()).log(Level.SEVERE, null, ex);
            lasterror = "Unknow error " + ex.getMessage();
        }
        return false;

    }

    public final String getLastMessage() {
        return lasterror;
    }

    public Optional<ProjectCollection> getAllProjects() {
        try {
            String url = baseUrl + context + "/project/search?expand=lead";
            String json = executeGet(url);

            if (json.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new ProjectCollection(new JSONObject(json)));
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    public Optional<Project> getProjectFromId(String projectId) {
        try {
            String url = baseUrl + context + "/project/" + projectId + "?expand=lead";
            String json = executeGet(url);
            if (json.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new Project(new JSONObject(json)));
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    public Optional<RolesUrls> getProjectRolesUrls(String projectKey) {
        try {
            String url = baseUrl + context + "/project/" + projectKey + "/role";
            String json = executeGet(url);
            if (json.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new RolesUrls(new JSONObject(json)));
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    public Optional<RoleCollection> getUsersWithRole(String url) {
        try {
            String json = executeGet(url);
            if (json.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new RoleCollection(new JSONObject(json)));
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    public Optional<JiraUser> getUserFromAccountId(String accountId) {
        String url = baseUrl + context + "/user/?accountId=" + accountId;
        try {

            String json = this.executeGet(url);
            if (!json.isEmpty()) {
                JiraUser usr = new JiraUser(new JSONObject(json));
                return Optional.of(usr);
            }
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.WARNING, ex.getMessage());
        } catch (JSONException ex) {
            LOG.warning(ex.getLocalizedMessage());
        }
        return Optional.empty();
    }

    public Optional<IssueCollection> getIssuesForProject(String projectId) {
        try {
            String url = baseUrl + context + "/search?jql=project=" + projectId + "&fields=*all&expand=transitions,transitions.fields,names,changelog";
            String json = executeGet(url);
            if (json.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new IssueCollection(projectId, new JSONObject(json), this));
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    public Optional<IssueCollection> getMoreIssues(IssueCollection collection) {
        //search issues does not return all the pagination elements for some reason
        try {
            String url = baseUrl + context + "/search?jql=project="
                    + collection.getKey() + "&startAt=" + (collection.getStartAt() + collection.getSize()) + "&fields=*all&expand=transitions,transitions.fields,names,changelog";
            String json = executeGet(url);

            return Optional.of(new IssueCollection(collection.getKey(), new JSONObject(json), this));
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    Optional<JiraUserCollection> getUsersInGroup(String groupname) {
        try {
            String escaped = UrlEscapers.urlFragmentEscaper().escape(groupname).replace("/", "%252F");
            String url = baseUrl + context + "/group/member?groupname=" + escaped;
            String json = executeGet(url);

            if (json.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new JiraUserCollection(new JSONObject(json)));
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    public byte[] downloadFileContent(String url) {
        try {

            byte[] json = executeStreamGetWithBasicAuth(url);
            //    System.err.println(json);
            return json;
        } catch (JSONException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new byte[0];
    }

    //get more items
    public final <T extends Pagination> Optional<T> getMore(T from) {
        try {
            int startAt = from.getStartAt();
            int nextStart = startAt + from.getSize();
            if (nextStart != startAt) {
                String fromStr = "startAt=" + startAt;
                String toStr = "startAt=" + nextStart;
                String url = from.getSelf();
                if (url != null && !url.isEmpty()) {
                    url = url.replace(fromStr, toStr);
                    String json = executeGet(url);
                    if (json.isEmpty()) {
                        return Optional.empty();
                    }
                    JSONObject obj = new JSONObject(json);
                    return Optional.of(from.create(obj));
                }
            }

        } catch (JSONException | NotFoundException ex) {
            Logger.getLogger(ConfluenceRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    public Optional<JiraComment> getCommentFromUrl(String self) {
        try {

            String json = executeGet(self);
            if (json == null || json.isEmpty()) {
                return Optional.empty();
            }
            JiraComment c = new JiraComment(new JSONObject(json));
            return Optional.of(c);
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.WARNING, ex.getMessage());
        }
        return Optional.empty();
    }

    public Optional<Issue> downloadIssueFromUrl(String self) {
        try {
            String url = self + "?fields=*all&expand=transitions,transitions.fields,names,changelog";
            String json = executeGet(url);
            if (json == null || json.isEmpty()) {
                return Optional.empty();
            }
            Issue issue = new Issue(new JSONObject(json), this);
            return Optional.of(issue);
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.WARNING, ex.getMessage());
        }
        return Optional.empty();
    }

    public Optional<JiraAttachment> getAttachmentFromUrl(String self) {
        try {
            String json = executeGet(self);
            if (json == null || json.isEmpty()) {
                return Optional.empty();
            }
            JiraAttachment att = new JiraAttachment(new JSONObject(json));
            return Optional.of(att);
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.WARNING, ex.getMessage());
        }
        return Optional.empty();
    }

    public void addLabel(Issue issue, String label) {
        String url = baseUrl + context + "/issue/" + issue.getId();

        //'{"update": {"labels": [{"add": "Label_10"}]}}'
        JSONObject root = new JSONObject();
        JSONObject labelsObj = new JSONObject();
        JSONArray labelsarr = new JSONArray();
        root.put("update", labelsObj);
        labelsObj.put("labels", labelsarr);
        JSONObject toPut = new JSONObject();
        toPut.put("add", label);
        labelsarr.put(toPut);
        String jsonBody = root.toString();
        try {
            executePutWithPayload(url, jsonBody);
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Optional<WorkflowCollection> getWorkflows() {
        String url = baseUrl + context + "/workflow";

        try {

            String json = executeGet(url);
            if (json == null || json.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new WorkflowCollection(new JSONArray(json)));
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.WARNING, ex.getMessage());;
        }
        return Optional.empty();
    }

    public Optional<WorkflowScheme> getWorkflowSchemes(Project proj) {
        String url = baseUrl + context + "/project/" + proj.getKey() + "/workflowscheme";

        try {

            String json = executeGet(url);
            if (json == null || json.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new WorkflowScheme(new JSONObject(json)));
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.WARNING, ex.getMessage());
        }
        return Optional.empty();
    }

    public Optional<WorkflowSchemeCollection> getAllWorkflowSchemes() {
        String url = baseUrl + context + "/workflowscheme";

        try {

            String json = executeGet(url);
            if (json == null || json.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new WorkflowSchemeCollection(new JSONObject(json)));
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.WARNING, ex.getMessage());
        }
        return Optional.empty();
    }

    public String getTransitionProperties(Transition t) {
        try {
            String url = baseUrl + context + "/workflow/transitions/" + t.getId() + "/properties";
            String json = executeGet(url);
            return json;
        } catch (NotFoundException ex) {
            Logger.getLogger(JiraRest.class.getName()).log(Level.WARNING, ex.getMessage());
        }
        return "";
    }
}
